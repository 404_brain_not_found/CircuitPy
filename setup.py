import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Ciruitpy",
    version="0.0.1",
    author="404_brain_not_found",
    author_email="thom.quirk180@gmail.com",
    description="A module to do the most circuit equations",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/thom.quirk180/CircuitPy",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'numpy',
        'pandas',
        'openpyxl',
        'python-docx',
        'tqdm',
        'pyvisa'
    ]
)